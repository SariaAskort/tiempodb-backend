package com.alderaeney.tiempodbbackend.xmlcontroller;

import com.alderaeney.tiempodbbackend.model.Day;
import com.alderaeney.tiempodbbackend.model.Information;
import com.alderaeney.tiempodbbackend.model.Locality;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class CtlFile extends CtlDom {
    private static final String ET_DATA = "data";
    private static final String ET_INFORMATION = "information";
    private static final String ET_LOCALITY = "locality";
    private static final String ET_DAY1 = "day1";
    private static final String ET_DAY2 = "day2";
    private static final String ET_DAY3 = "day3";
    private static final String ET_DAY4 = "day4";
    private static final String ET_DAY5 = "day5";
    private static final String ET_DAY6 = "day6";
    private static final String ET_DAY7 = "day7";
    private Locality locality = new Locality();

    public CtlFile(Locality locality) {
        this.locality = locality;
    }

    public CtlFile() {
    }

    public static Document recuperar(File xmlFile) throws IOException, SAXException, ParserConfigurationException {
        Document doc = instanciarDocument(xmlFile);
        return doc;
    }

    public static Locality read(Document doc) {
        Information information = new Information();
        Locality locality = new Locality();
        ArrayList<Day> days = new ArrayList<Day>();
        Day day = null;

        Element arrel = doc.getDocumentElement();

        NodeList nodeList = arrel.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                if (nodeList.item(i).getNodeName().equals(ET_INFORMATION)) {
                    information = CtlInformation.read((Element) nodeList.item(i));
                } else if (nodeList.item(i).getNodeName().equals(ET_LOCALITY)) {
                    locality = CtlLocality.read((Element) nodeList.item(i));
                } else if (nodeList.item(i).getNodeName().equals(ET_DAY1)) {
                	day = CtlDay.read((Element) nodeList.item(i));
                	day.setLocality(locality);
                    days.add(day);
                } else if (nodeList.item(i).getNodeName().equals(ET_DAY2)) {
                	day = CtlDay.read((Element) nodeList.item(i));
                	day.setLocality(locality);
                    days.add(day);
                } else if (nodeList.item(i).getNodeName().equals(ET_DAY3)) {
                	day = CtlDay.read((Element) nodeList.item(i));
                	day.setLocality(locality);
                    days.add(day);
                } else if (nodeList.item(i).getNodeName().equals(ET_DAY4)) {
                	day = CtlDay.read((Element) nodeList.item(i));
                	day.setLocality(locality);
                    days.add(day);
                } else if (nodeList.item(i).getNodeName().equals(ET_DAY5)) {
                	day = CtlDay.read((Element) nodeList.item(i));
                	day.setLocality(locality);
                    days.add(day);
                } else if (nodeList.item(i).getNodeName().equals(ET_DAY6)) {
                	day = CtlDay.read((Element) nodeList.item(i));
                	day.setLocality(locality);
                    days.add(day);
                } else if (nodeList.item(i).getNodeName().equals(ET_DAY7)) {
                	day = CtlDay.read((Element) nodeList.item(i));
                	day.setLocality(locality);
                    days.add(day);
                }
            }
        }
        information.setLocality(locality);
        locality.setInformation(information);
        locality.setDays(new HashSet<Day>(days));

        return locality;
    }
}
