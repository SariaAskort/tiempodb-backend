package com.alderaeney.tiempodbbackend.xmlcontroller;

import com.alderaeney.tiempodbbackend.model.Day;
import org.w3c.dom.Element;

import java.time.LocalDate;
import java.time.LocalTime;

public class CtlDay extends CtlDom {
    private static final String ET_DATE = "date";
    private static final String ET_TEMPERATURE_MAX = "temperature_max";
    private static final String ET_TEMPERATURE_MIN = "temperature_min";
    private static final String ET_TEXT = "text";
    private static final String ET_HUMIDITY = "humidity";
    private static final String ET_WIND = "wind";
    private static final String ET_WIND_DIRECTION = "wind_direction";
    private static final String ET_SUNRISE = "sunrise";
    private static final String ET_SUNSET = "sunset";
    private static final String ET_MOONRISE = "moonrise";
    private static final String ET_MOONSET = "moonset";

    public static Day read(Element elemDay) {
        LocalDate date = parseDate(getValorEtiqueta(ET_DATE, elemDay));
        float temperature_max = Float.parseFloat(getValorEtiqueta(ET_TEMPERATURE_MAX, elemDay));
        float temperature_min = Float.parseFloat(getValorEtiqueta(ET_TEMPERATURE_MIN, elemDay));
        String text = getValorEtiqueta(ET_TEXT, elemDay);
        int humidity = Integer.parseInt(getValorEtiqueta(ET_HUMIDITY, elemDay));
        int wind = Integer.parseInt(getValorEtiqueta(ET_WIND, elemDay));
        String wind_direction = getValorEtiqueta(ET_WIND_DIRECTION, elemDay);
        LocalTime sunrise = parseTime(getValorEtiqueta(ET_SUNRISE, elemDay));
        LocalTime sunset = parseTime(getValorEtiqueta(ET_SUNSET, elemDay));
        LocalTime moonrise = parseTime(getValorEtiqueta(ET_MOONRISE, elemDay));
        LocalTime moonset = parseTime(getValorEtiqueta(ET_MOONSET, elemDay));

        return new Day(date, temperature_max, temperature_min, text, humidity, wind, wind_direction, sunrise, sunset, moonrise, moonset);
    }

    private static LocalTime parseTime(String time) {
        String[] array = time.split(":");
        if (array[0].length() == 1) {
            array[0] = "0" + array[0];
        }
        if (array[1].length() == 1) {
            array[1] = "0" + array[1];
        }
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm", Locale.ENGLISH);
        return LocalTime.parse(array[0] + ":" + array[1]);
    }

    private static LocalDate parseDate(String date) {
        String[] array = date.split("-");
        if (array[1].length() == 1) {
            array[1] = "0" + array[1];
        }
        if (array[2].length() == 1) {
            array[2] = "0" + array[2];
        }
        return LocalDate.parse(array[0] + "-" + array[1] + "-" + array[2]);
    }
}
