package com.alderaeney.tiempodbbackend.xmlcontroller;

import com.alderaeney.tiempodbbackend.model.Locality;
import org.w3c.dom.Element;

public class CtlLocality extends CtlDom {
    private static final String ET_NAME = "name";
    private static final String ET_COUNTRY = "country";
    private static final String ET_LID = "lid";

    public static Locality read(Element elemLocality) {
        String name = getValorEtiqueta(ET_NAME, elemLocality);
        String country = getValorEtiqueta(ET_COUNTRY, elemLocality);
        Integer lid = null;
        try {
            lid = Integer.valueOf(getValorEtiqueta(ET_LID, elemLocality));
        } catch (Exception e) {
            return new Locality(name, country);
        }

        return new Locality(name, country, lid);
    }
}
