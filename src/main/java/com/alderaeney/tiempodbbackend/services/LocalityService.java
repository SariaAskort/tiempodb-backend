package com.alderaeney.tiempodbbackend.services;

import com.alderaeney.tiempodbbackend.repositories.LocalityRepository;
import com.alderaeney.tiempodbbackend.model.Locality;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocalityService {

    private final LocalityRepository localityRepository;

    @Autowired
    public LocalityService(LocalityRepository localityRepository) {
        this.localityRepository = localityRepository;
    }

    public List<Locality> getLocalities() {return localityRepository.findAll(); }

    public void deleteLocality(Long localityId) {
        boolean exists = localityRepository.existsById(localityId);
        if (!exists) {
            throw new IllegalStateException("Locality with id " + localityId + " doesn't exists");
        }

        localityRepository.deleteById(localityId);
    }

    public Locality getLocality(Long localityId) {
        boolean exists = localityRepository.existsById(localityId);
        if (!exists) throw new IllegalStateException("Locality with id " + localityId + " doesn't exists");

        return localityRepository.findLocalityById(localityId);
    }

    public void updateLocality(Locality locality) {
        localityRepository.save(locality);
    }

}
