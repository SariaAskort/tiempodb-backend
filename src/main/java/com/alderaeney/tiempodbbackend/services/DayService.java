package com.alderaeney.tiempodbbackend.services;

import com.alderaeney.tiempodbbackend.model.Day;
import com.alderaeney.tiempodbbackend.repositories.DayRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DayService {

    private DayRepository dayRepository;

    @Autowired
    public DayService(DayRepository dayRepository) {
        this.dayRepository = dayRepository;
    }

    public void insertDay(Day day) {
        dayRepository.save(day);
    }

}
