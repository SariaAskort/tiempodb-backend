package com.alderaeney.tiempodbbackend.config;

import com.alderaeney.tiempodbbackend.repositories.LocalityRepository;
import com.alderaeney.tiempodbbackend.model.Locality;
import com.alderaeney.tiempodbbackend.xmlcontroller.CtlFile;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.w3c.dom.Document;

import java.io.File;
import java.util.ArrayList;

@Configuration
public class InitialConfig {

    @Bean
    CommandLineRunner commandLineRunner(LocalityRepository repository) {
        return args -> {
            File dataFolder = new File("src/main/resources/static/BaseXML");

            ArrayList<Locality> localities = new ArrayList<>();

            for (File xml : dataFolder.listFiles()) {
                Document doc = CtlFile.recuperar(xml);
                localities.add(CtlFile.read(doc));
            }

            repository.saveAll(localities);
        };
    }

}
