package com.alderaeney.tiempodbbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "Day")
public class Day implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "temperature_max")
    private Float temperature_max;

    @Column(name = "temperature_min")
    private Float temperature_min;

    @Column(name = "text")
    private String text;

    @Column(name = "humidity")
    private int humidity;

    @Column(name = "wind")
    private int wind;

    @Column(name = "wind_direction")
    private String wind_direction;

    @Column(name = "sunrise")
    private LocalTime sunrise;

    @Column(name = "sunset")
    private LocalTime sunset;

    @Column(name = "moonrise")
    private LocalTime moonrise;

    @Column(name = "moonset")
    private LocalTime moonset;

    @ManyToOne
    @JoinColumn(name = "localityId")
    @JsonIgnore
    private Locality locality;

    public Day(LocalDate date,
               Float temperature_max,
               Float temperature_min,
               String text,
               int humidity,
               int wind,
               String wind_direction,
               LocalTime sunrise,
               LocalTime sunset,
               LocalTime moonrise,
               LocalTime moonset) {
        this.date = date;
        this.temperature_max = temperature_max;
        this.temperature_min = temperature_min;
        this.text = text;
        this.humidity = humidity;
        this.wind = wind;
        this.wind_direction = wind_direction;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.moonrise = moonrise;
        this.moonset = moonset;
    }

    public Day(Long id,
               LocalDate date,
               Float temperature_max,
               Float temperature_min,
               String text,
               int humidity,
               int wind,
               String wind_direction,
               LocalTime sunrise,
               LocalTime sunset,
               LocalTime moonrise,
               LocalTime moonset) {
        this.id = id;
        this.date = date;
        this.temperature_max = temperature_max;
        this.temperature_min = temperature_min;
        this.text = text;
        this.humidity = humidity;
        this.wind = wind;
        this.wind_direction = wind_direction;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.moonrise = moonrise;
        this.moonset = moonset;
    }

    public Day() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Float getTemperature_max() {
        return temperature_max;
    }

    public void setTemperature_max(Float temperature_max) {
        this.temperature_max = temperature_max;
    }

    public Float getTemperature_min() {
        return temperature_min;
    }

    public void setTemperature_min(Float temperature_min) {
        this.temperature_min = temperature_min;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getWind() {
        return wind;
    }

    public void setWind(int wind) {
        this.wind = wind;
    }

    public String getWind_direction() {
        return wind_direction;
    }

    public void setWind_direction(String wind_direction) {
        this.wind_direction = wind_direction;
    }

    public LocalTime getSunrise() {
        return sunrise;
    }

    public void setSunrise(LocalTime sunrise) {
        this.sunrise = sunrise;
    }

    public LocalTime getSunset() {
        return sunset;
    }

    public void setSunset(LocalTime sunset) {
        this.sunset = sunset;
    }

    public LocalTime getMoonrise() {
        return moonrise;
    }

    public void setMoonrise(LocalTime moonrise) {
        this.moonrise = moonrise;
    }

    public LocalTime getMoonset() {
        return moonset;
    }

    public void setMoonset(LocalTime moonset) {
        this.moonset = moonset;
    }

    public Locality getLocality() {
        return locality;
    }

    public void setLocality(Locality locality) {
        this.locality = locality;
    }

    @Override
    public String toString() {
        return "Day{" +
                "id=" + id +
                ", date=" + date +
                ", temperature_max=" + temperature_max +
                ", temperature_min=" + temperature_min +
                ", text='" + text + '\'' +
                ", humidity=" + humidity +
                ", wind=" + wind +
                ", wind_direction='" + wind_direction + '\'' +
                ", sunrise=" + sunrise +
                ", sunset=" + sunset +
                ", moonrise=" + moonrise +
                ", moonset=" + moonset +
                '}';
    }
}
