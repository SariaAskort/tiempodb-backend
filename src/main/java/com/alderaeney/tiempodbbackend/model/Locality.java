package com.alderaeney.tiempodbbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "Locality")
public class Locality implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "country")
    private String country;

    @Column(name = "lid")
    @JsonIgnore
    private Integer lid;

    @OneToMany(mappedBy = "locality", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Day> days;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "locality", fetch = FetchType.LAZY)
    private Information information;

    public Locality(Long id, String name, String country, Integer lid) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.lid = lid;
    }

    public Locality(String name, String country, Integer lid) {
        this.name = name;
        this.country = country;
        this.lid = lid;
    }

    public Locality(String name, String country) {
        this.name = name;
        this.country = country;
    }

    public Locality() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Set<Day> getDays() {
        return days;
    }

    public void setDays(Set<Day> days) {
        this.days = days;
    }

    public Information getInformation() {
        return information;
    }

    public void setInformation(Information information) {
        this.information = information;
    }

    public Integer getLid() {
        return lid;
    }

    public void setLid(Integer lid) {
        this.lid = lid;
    }

    @Override
    public String toString() {
        return "Locality{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", days=" + days +
                '}';
    }
}