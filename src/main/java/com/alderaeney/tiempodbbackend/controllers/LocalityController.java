package com.alderaeney.tiempodbbackend.controllers;

import com.alderaeney.tiempodbbackend.model.Day;
import com.alderaeney.tiempodbbackend.model.Locality;
import com.alderaeney.tiempodbbackend.services.DayService;
import com.alderaeney.tiempodbbackend.services.LocalityService;
import com.alderaeney.tiempodbbackend.xmlcontroller.CtlFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(path = "api/v1/localities")
public class LocalityController {

    private final String apiKey = "";

    File tmpFolder = new File("src/main/resources/tmp/");

    private final LocalityService localityService;

    private final DayService dayService;

    @Autowired
    public LocalityController(LocalityService localityService, DayService dayService) {
        this.localityService = localityService;
        this.dayService = dayService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Locality>> getLocalities() {
        List<Locality> localities = localityService.getLocalities();
        return new ResponseEntity<>(localities, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Locality> getLocality(@PathVariable("id") Long id) {
        Locality locality = localityService.getLocality(id);
        return new ResponseEntity<>(locality, HttpStatus.OK);
    }

    @GetMapping("/{id}/delete/")
    public ResponseEntity<List<Locality>> deleteLocality(@PathVariable("id") Long id) {
        localityService.deleteLocality(id);
        List<Locality> localities = localityService.getLocalities();
        return new ResponseEntity<>(localities, HttpStatus.OK);
    }

    @GetMapping("/{id}/retrieve")
    public ResponseEntity<Locality> retrieveMoreDays(@PathVariable("id") Long id) {
        Locality locality = null;
        try {
            locality = localityService.getLocality(id);
            final String uri = "https://api.tutiempo.net/xml/?lan=es&apid=" + apiKey + "&lid=" + locality.getLid();

            RestTemplate restTemplate = new RestTemplate();
            String result = restTemplate.getForObject(uri, String.class);

            File tmpFile = new File(tmpFolder + "/tmpFile");

            FileWriter fileWriter = new FileWriter(tmpFile);
            fileWriter.write(result);
            fileWriter.close();

            Document doc = CtlFile.recuperar(tmpFile);
            Locality aux = CtlFile.read(doc);

            tmpFile.delete();

            for (Day day :
                    aux.getDays()) {
                day.setLocality(locality);
                locality.getDays().add(day);
            }

            localityService.updateLocality(locality);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(locality, HttpStatus.OK);
    }
}
