package com.alderaeney.tiempodbbackend.repositories;

import com.alderaeney.tiempodbbackend.model.Locality;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LocalityRepository extends JpaRepository<Locality, Long> {

    @Query("SELECT s FROM Locality s WHERE s.id = ?1")
    Locality findLocalityById(Long localityId);
}
